﻿namespace Avtopark
{
    partial class XtraForm4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.vudtransportaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.avtoparkDataSet = new Avtopark.AvtoparkDataSet();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.controlNavigator2 = new DevExpress.XtraEditors.ControlNavigator();
            this.controlNavigator1 = new DevExpress.XtraEditors.ControlNavigator();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colkod_vuda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colkod_parka = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazva_vuda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvmistumist = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmicnist_dvugyna = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvutrata_paluva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfoto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.vudtransportaavtobusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colkod_avtobusa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colkod_vudy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colgosnomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrik_vupyska = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnomer_dvugyna = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vud_transportaTableAdapter = new Avtopark.AvtoparkDataSetTableAdapters.vud_transportaTableAdapter();
            this.avtobusTableAdapter = new Avtopark.AvtoparkDataSetTableAdapters.avtobusTableAdapter();
            this.avtobusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vudtransportaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avtoparkDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vudtransportaavtobusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.avtobusBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(960, 641);
            this.splitContainerControl1.SplitterPosition = 313;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.label2);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.label1);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.controlNavigator2);
            this.layoutControl1.Controls.Add(this.controlNavigator1);
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(332, 355, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(313, 641);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.vudtransportaBindingSource, "micnist_dvugyna", true));
            this.textEdit3.Location = new System.Drawing.Point(118, 110);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(183, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 8;
            // 
            // vudtransportaBindingSource
            // 
            this.vudtransportaBindingSource.DataMember = "vud_transporta";
            this.vudtransportaBindingSource.DataSource = this.avtoparkDataSet;
            // 
            // avtoparkDataSet
            // 
            this.avtoparkDataSet.DataSetName = "AvtoparkDataSet";
            this.avtoparkDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(118, 445);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(183, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 322);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 48);
            this.label2.TabIndex = 7;
            this.label2.Text = "Автомобілі";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(118, 421);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(183, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 0;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(118, 469);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(183, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Види транспорту";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(118, 397);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(183, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 2;
            // 
            // controlNavigator2
            // 
            this.controlNavigator2.Location = new System.Drawing.Point(12, 374);
            this.controlNavigator2.Name = "controlNavigator2";
            this.controlNavigator2.Size = new System.Drawing.Size(283, 19);
            this.controlNavigator2.StyleController = this.layoutControl1;
            this.controlNavigator2.TabIndex = 5;
            this.controlNavigator2.Text = "controlNavigator2";
            // 
            // controlNavigator1
            // 
            this.controlNavigator1.Location = new System.Drawing.Point(12, 39);
            this.controlNavigator1.Name = "controlNavigator1";
            this.controlNavigator1.Size = new System.Drawing.Size(283, 19);
            this.controlNavigator1.StyleController = this.layoutControl1;
            this.controlNavigator1.TabIndex = 4;
            this.controlNavigator1.Text = "controlNavigator1";
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.vudtransportaBindingSource, "vmistumist", true));
            this.textEdit2.Location = new System.Drawing.Point(118, 86);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(183, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 1;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.vudtransportaBindingSource, "nazva_vuda", true));
            this.textEdit1.Location = new System.Drawing.Point(118, 62);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(183, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 0;
            // 
            // textEdit4
            // 
            this.textEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.vudtransportaBindingSource, "foto", true));
            this.textEdit4.Location = new System.Drawing.Point(118, 134);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.textEdit4.Size = new System.Drawing.Size(183, 174);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.TabStop = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(313, 641);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 300);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(293, 10);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 481);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(293, 140);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.controlNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(293, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.controlNavigator2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 362);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(293, 23);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(293, 27);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.label2;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 310);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(293, 52);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit1;
            this.layoutControlItem5.CustomizationFormText = "Назва виду";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem5.Text = "Назва виду";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit2;
            this.layoutControlItem6.CustomizationFormText = "Вантажопідйомність";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem6.Text = "Вантажопідйомність";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit3;
            this.layoutControlItem8.CustomizationFormText = "Об\'єм двигуна";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem8.Text = "Об\'єм двигуна";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEdit4;
            this.layoutControlItem10.CustomizationFormText = "Фото";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(293, 178);
            this.layoutControlItem10.Text = "Фото";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit7;
            this.layoutControlItem7.CustomizationFormText = "Вид";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 385);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem7.Text = "Вид";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit5;
            this.layoutControlItem9.CustomizationFormText = "Держ номер";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 409);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem9.Text = "Держ номер";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEdit6;
            this.layoutControlItem11.CustomizationFormText = "Номер двигуна";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 433);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem11.Text = "Номер двигуна";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit8;
            this.layoutControlItem12.CustomizationFormText = "Рік випуску";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 457);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem12.Text = "Рік випуску";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(102, 13);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainer1.Size = new System.Drawing.Size(642, 641);
            this.splitContainer1.SplitterDistance = 294;
            this.splitContainer1.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.vudtransportaBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(642, 294);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colkod_vuda,
            this.colkod_parka,
            this.colnazva_vuda,
            this.colvmistumist,
            this.colmicnist_dvugyna,
            this.colvutrata_paluva,
            this.colfoto});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colkod_vuda
            // 
            this.colkod_vuda.FieldName = "kod_vuda";
            this.colkod_vuda.Name = "colkod_vuda";
            this.colkod_vuda.Visible = true;
            this.colkod_vuda.VisibleIndex = 0;
            // 
            // colkod_parka
            // 
            this.colkod_parka.FieldName = "kod_parka";
            this.colkod_parka.Name = "colkod_parka";
            this.colkod_parka.Visible = true;
            this.colkod_parka.VisibleIndex = 1;
            // 
            // colnazva_vuda
            // 
            this.colnazva_vuda.FieldName = "nazva_vuda";
            this.colnazva_vuda.Name = "colnazva_vuda";
            this.colnazva_vuda.Visible = true;
            this.colnazva_vuda.VisibleIndex = 2;
            // 
            // colvmistumist
            // 
            this.colvmistumist.FieldName = "vmistumist";
            this.colvmistumist.Name = "colvmistumist";
            this.colvmistumist.Visible = true;
            this.colvmistumist.VisibleIndex = 3;
            // 
            // colmicnist_dvugyna
            // 
            this.colmicnist_dvugyna.FieldName = "micnist_dvugyna";
            this.colmicnist_dvugyna.Name = "colmicnist_dvugyna";
            this.colmicnist_dvugyna.Visible = true;
            this.colmicnist_dvugyna.VisibleIndex = 4;
            // 
            // colvutrata_paluva
            // 
            this.colvutrata_paluva.FieldName = "vutrata_paluva";
            this.colvutrata_paluva.Name = "colvutrata_paluva";
            this.colvutrata_paluva.Visible = true;
            this.colvutrata_paluva.VisibleIndex = 5;
            // 
            // colfoto
            // 
            this.colfoto.FieldName = "foto";
            this.colfoto.Name = "colfoto";
            this.colfoto.Visible = true;
            this.colfoto.VisibleIndex = 6;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.avtobusBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(642, 343);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // vudtransportaavtobusBindingSource
            // 
            this.vudtransportaavtobusBindingSource.DataMember = "vud_transportaavtobus";
            this.vudtransportaavtobusBindingSource.DataSource = this.vudtransportaBindingSource;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colkod_avtobusa,
            this.colkod_vudy,
            this.colgosnomer,
            this.colrik_vupyska,
            this.colnomer_dvugyna});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // colkod_avtobusa
            // 
            this.colkod_avtobusa.FieldName = "kod_avtobusa";
            this.colkod_avtobusa.Name = "colkod_avtobusa";
            this.colkod_avtobusa.Visible = true;
            this.colkod_avtobusa.VisibleIndex = 0;
            // 
            // colkod_vudy
            // 
            this.colkod_vudy.FieldName = "kod_vudy";
            this.colkod_vudy.Name = "colkod_vudy";
            this.colkod_vudy.Visible = true;
            this.colkod_vudy.VisibleIndex = 1;
            // 
            // colgosnomer
            // 
            this.colgosnomer.FieldName = "gosnomer";
            this.colgosnomer.Name = "colgosnomer";
            this.colgosnomer.Visible = true;
            this.colgosnomer.VisibleIndex = 2;
            // 
            // colrik_vupyska
            // 
            this.colrik_vupyska.FieldName = "rik_vupyska";
            this.colrik_vupyska.Name = "colrik_vupyska";
            this.colrik_vupyska.Visible = true;
            this.colrik_vupyska.VisibleIndex = 3;
            // 
            // colnomer_dvugyna
            // 
            this.colnomer_dvugyna.FieldName = "nomer_dvugyna";
            this.colnomer_dvugyna.Name = "colnomer_dvugyna";
            this.colnomer_dvugyna.Visible = true;
            this.colnomer_dvugyna.VisibleIndex = 4;
            // 
            // vud_transportaTableAdapter
            // 
            this.vud_transportaTableAdapter.ClearBeforeFill = true;
            // 
            // avtobusTableAdapter
            // 
            this.avtobusTableAdapter.ClearBeforeFill = true;
            // 
            // avtobusBindingSource
            // 
            this.avtobusBindingSource.DataMember = "avtobus";
            this.avtobusBindingSource.DataSource = this.avtoparkDataSet;
            // 
            // XtraForm4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 641);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "XtraForm4";
            this.Text = "Транспорт";
            this.Load += new System.EventHandler(this.XtraForm4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vudtransportaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avtoparkDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vudtransportaavtobusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.avtobusBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.ControlNavigator controlNavigator2;
        private DevExpress.XtraEditors.ControlNavigator controlNavigator1;
        private DevExpress.XtraEditors.PictureEdit textEdit4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private AvtoparkDataSet avtoparkDataSet;
        private System.Windows.Forms.BindingSource vudtransportaBindingSource;
        private AvtoparkDataSetTableAdapters.vud_transportaTableAdapter vud_transportaTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colkod_vuda;
        private DevExpress.XtraGrid.Columns.GridColumn colkod_parka;
        private DevExpress.XtraGrid.Columns.GridColumn colnazva_vuda;
        private DevExpress.XtraGrid.Columns.GridColumn colvmistumist;
        private DevExpress.XtraGrid.Columns.GridColumn colmicnist_dvugyna;
        private DevExpress.XtraGrid.Columns.GridColumn colvutrata_paluva;
        private DevExpress.XtraGrid.Columns.GridColumn colfoto;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource vudtransportaavtobusBindingSource;
        private AvtoparkDataSetTableAdapters.avtobusTableAdapter avtobusTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colkod_avtobusa;
        private DevExpress.XtraGrid.Columns.GridColumn colkod_vudy;
        private DevExpress.XtraGrid.Columns.GridColumn colgosnomer;
        private DevExpress.XtraGrid.Columns.GridColumn colrik_vupyska;
        private DevExpress.XtraGrid.Columns.GridColumn colnomer_dvugyna;
        private System.Windows.Forms.BindingSource avtobusBindingSource;

    }
}